import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthViewComponent } from './views/auth-view/auth-view.component';
import { NotFoundViewComponent } from './views/not-found-view/not-found-view.component';
import { DetailsSerieViewComponent } from './views/details-serie-view/details-serie-view.component';
import { SeriesViewComponent } from './views/series-view/series-view.component';
import { SingleSerieViewComponent } from './single-serie-view/single-serie-view.component';
import {SerieService} from "./services/serie/serie.service";
import {AuthService} from "./services/auth/auth.service";
import { SeriesTableComponent } from './components/series-table/series-table.component';
import { SerieTableRowComponent } from './components/serie-table-row/serie-table-row.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthViewComponent,
    NotFoundViewComponent,
    DetailsSerieViewComponent,
    SeriesViewComponent,
    SingleSerieViewComponent,
    SeriesTableComponent,
    SerieTableRowComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [
    SerieService,
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
