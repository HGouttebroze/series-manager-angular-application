import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsSerieViewComponent } from './details-serie-view.component';

describe('DetailsSerieViewComponent', () => {
  let component: DetailsSerieViewComponent;
  let fixture: ComponentFixture<DetailsSerieViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailsSerieViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsSerieViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
