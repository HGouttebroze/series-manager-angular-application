import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
//import {AuthViewComponent} from "../../../../correction-books-app/3.7/BooksManager/src/app/auth-view/auth-view.component";
import {AuthGuardService} from "../../../../correction-books-app/3.7/BooksManager/src/app/services/auth-guard/auth-guard.service";
//import {BooksViewComponent} from "../../../../correction-books-app/3.7/BooksManager/src/app/books-view/books-view.component";
//import {SingleBookViewComponent} from "../../../../correction-books-app/3.7/BooksManager/src/app/single-book-view/single-book-view.component";
import {ErrorViewComponent} from "../../../../correction-books-app/3.7/BooksManager/src/app/error-view/error-view.component";
import {AuthViewComponent} from "./views/auth-view/auth-view.component";
import {SeriesViewComponent} from "./views/series-view/series-view.component";
import {SingleSerieViewComponent} from "./single-serie-view/single-serie-view.component";


const routes: Routes = [
  { path: 'auth', component:  AuthViewComponent},
  { path: 'series', canActivate: [AuthGuardService], component: SeriesViewComponent},
  { path: 'serie/:id', canActivate: [AuthGuardService], component: SingleSerieViewComponent},
  { path: '', canActivate: [AuthGuardService], component: SeriesViewComponent },
  { path: 'not-found', component: ErrorViewComponent },
  { path: '**', redirectTo: 'not-found' }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
