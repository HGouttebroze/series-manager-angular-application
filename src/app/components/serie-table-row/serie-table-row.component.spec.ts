import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SerieTableRowComponent } from './serie-table-row.component';

describe('SerieTableRowComponent', () => {
  let component: SerieTableRowComponent;
  let fixture: ComponentFixture<SerieTableRowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SerieTableRowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SerieTableRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
