# Create a Merge Request from a new Issue with GIT
## Check out, review, and merge locally
###Step 1. Fetch and check out the branch for this merge request

`git fetch origin`

`git checkout -b "1-create-authentification" "origin/1-create-authentification"`
 
Step 2. Review the changes locally

`git fetch origin``git checkout "master"`

`git merge --no-ff "1-create-authentification"`


Step 3. Merge the branch and fix any conflicts that come up


Step 4. Push the result of the merge to GitLab

`git push origin "master"`

Tip: You can also checkout merge requests locally by following these guidelines
